import React from 'react'
import FacebookLogin from 'react-facebook-login';
import GoogleLogin from 'react-google-login';

import styles from './styles.module.scss'

function App() {
  const componentClicked = () => {
    console.log('componentClicked')
  }

  const responseFacebook = () => {
    console.log('responseFacebook')
  }

  const responseGoogle = () => {
    console.log('responseGoogle')
  }

  return (
    <div className={styles.appContainer}>
      <div className={styles.content}>
        <div className={styles.title}>Postcard</div>
        <div className={styles.authForm}>
          <input type="text" name="text" placeholder="Введите логин"/>
          <input type="password" name="text" placeholder="Введите пароль" />
          <button className={styles.loginButton}>Войти</button>
        </div>
        <FacebookLogin
          appId="1088597931155576"
          autoLoad={true}
          fields="name,email,picture"
          callback={responseFacebook}
          onClick={componentClicked}
          cssClass={styles.buttonFB}
          icon="fa-facebook"
        />
        <div className={styles.buttonGoogleWrapper}>
          <GoogleLogin
            clientId="658977310896-knrl3gka66fldh83dao2rhgbblmd4un9.apps.googleusercontent.com"
            buttonText="Login with Google"
            onSuccess={responseGoogle}
            onFailure={responseGoogle}
            cookiePolicy={'single_host_origin'}
            style={{}}
            className={styles.buttonGoogle}
          />
        </div>
      </div>
    </div>
  );
}

export default App;